#!/usr/local/bin/python3
### odtud už je to Vaše
import numpy as np
import pandas as pd
import sklearn as skit
import matplotlib.pyplot as plt
import seaborn as sns
import requests
from bs4 import BeautifulSoup
import re
from collections import OrderedDict


def retrieve_page(url, year, page):
    # Metropolitni Univerzita : nacitavanie 
    dll = "l.dll?"
    parameters = {
        'h~' : '',
        'DD' : '56',
        'TF' : 'K',
        'P1' : '19',
        'V1' : 'r',
        'H1' : '2017',
        'J1' : '.',
        'L1' : '1',
        'P2' : '2',
        'V2' : 'z',
        'H2' : '',
        'J2' : '.',
        'L2' : '1',
        'P3' : '3',
        'V3' : 'z',
        'H3' : '',
        'J3' : '.',
        'L3' : '1',
        'P4' : '4',
        'V4' : 'z',
        'H4' : '',
        'J4' : '.',
        'L4' : '1',
        'P5' : ' ',
        'V5' : 'z',
        'H5' : '',
        'J5' : '.',
        'VSE' : 'on',
        'TL' : 'MR21',
        'Pg' : str(page)
    }
    r = requests.get(url + dll, params=parameters)
    print("parsing from URL: " + r.url + "\n")
    return r.text

def get_marcs(content):
    bs = BeautifulSoup(content, features="html5lib")
    table = bs.find("table",{"class":"vse"}) 
    # print(table)
    vse = table.findAll("font")
    marcs = list(f.get_text() for f in vse)
    # for m in marcs:
    #     print(m)
    return marcs

def parse_marc(marc):
    temp = {}
    for s in marc.splitlines():
        code = s[:3]
        print(s)
        meat = re.split("[\$][a-z0-9]", s[7:])
        if(code == "008"):
            temp["lang"] = s[-5:-2]
#             print(temp["lang"])
        if(code == "100"):
            temp["author"] = re.split("[\$][a-z0-9]", s[5:])[1].replace(',' ,'')
#             print(author)
        if(code == "245"):
            split = re.split("[\$][a-z0-9]", s[7:])
            if(temp["lang"] == "eng"):
                temp["name_eng"] = split[1][:-2]
                temp["name_czk"] = split[2][:-2]
            if(temp["lang"] == "cze"):
                temp["name_czk"] = split[1][:-2]
                temp["name_eng"] = split[2][:-2]
#             print(split)
            helpers = split[len(split)-1].split(";")
            temp["leader"] = helpers[1].replace("vedoucí práce", '').strip()
            temp["oponent"] = helpers[2].replace("oponent", '').strip()
        if(code == "264"):
            temp["place"] = meat[1].replace(",", "")
            temp["year"] = meat[2]
        if(code == "300"):
            pages = re.match("[0-9]+", meat[1]).group(0)
            temp["pages"] = pages
        if(code == "502"):
            spl = meat[1].split("--")
            temp["thesis"] = spl[0].strip()
            temp["school"] = spl[1].split(",")[0]
            temp["faculty"] = spl[1].split(",")[2]
    # print(temp)
    return temp

def main():
    url = 'https://s-knihovna.mup.cz/katalog/'

    df_all = pd.DataFrame
    for year in range(2016, 2017):
        for page in range(1,2):
            page_html = retrieve_page(url, year, page)
            marcs = get_marcs(page_html)
            data = [parse_marc(d) for d in marcs]
            # print(data)
            df = pd.DataFrame(data)
            pd.concat([df_all, df])
            df
    print(df_all)
    df_all
    
    # df
    # print(df)
    # for m in marcs:
    #     d = parse_marc(m)
    #     print(d)
        # df = pd.DataFrame(d)
        # print(df)

if __name__== "__main__":
  main()
